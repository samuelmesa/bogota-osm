# Proyecto de Mapa base para Bogotá a partir de los datos de OpenStreetMap. 

Proyecto desarrollado en QGIS Desktop, versión 2.14.x.

Basado en el trabajo de [Charley Glynn](https://github.com/charleyglynn/OSM-Shapefile-QGIS-stylesheets)

Mapa base para el [Visor Geográfico Ambiental](http://www.secretariadeambiente.gov.co/visorgeo) de la Secretaría Distrital de Ambente de Bogotá (Colombia)

## Visualización del proyecto

Para visualizar el proyecto en el Visor de Mapas de Leaflet, consulte el siguiente enlace Web:

http://samuelmesa.gitlab.io/bogota-osm/mapa.html

## Contribuciones

* Samuel Fernando Mesa
* Diego Arcesio Rodríguez

